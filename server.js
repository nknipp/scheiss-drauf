'use strict'

const fs = require('fs')
const http = require('http')
const mime = require('mime')
const MongoClient = require('mongodb').MongoClient
const path = require('path')
const qs = require('querystring')
const url = require('url')
const util = require('util')

const readFileAsync = util.promisify(fs.readFile)

const port = process.env.PORT || 3000

MongoClient.connect(process.env.MONGODB_ADDON_URI, (err, db) => {
  if (err) return console.log(err);

  http.createServer(async (req, res) => {
    const host = req.headers['host']
    const hostname = host.split(':', 1)[0]
    if (!hostname.startsWith('www.')) {
      redirect(`//www.${host}`, res)
    }

    const reqUrl = url.parse(req.url)
    try {
      switch (reqUrl.pathname) {
        case '/q':
          await redirectToDrivingschool(db, req, res)
          break

        case '/':
          await responseClientFile(`${hostname}.html`, res)
          break

        default:
          await responseClientFile(reqUrl.pathname, res)
      }
    } catch (e) {
      console.log(e)
      res.statusCode = 500
      res.end('<html></html><body>Es ist ein interner Fehler aufgetreten.</body>')
    }
  }).listen(port, () => {
    console.log(`Server running at ${port}`)
  })
});

function parsePostData(req) {
  return new Promise((resolve, reject) => {
    if (req.method === 'POST') {
      let body = ''

      req.on('data', (data) => {
        body += data

        // Too much POST data, kill the connection!
        // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
        if (body.length > 1e6) {
          req.connection.destroy()
          reject()
        }
      })

      req.on('end', () => {
        const parsedBody = qs.parse(body)
        resolve(parsedBody)
      })
    } else {
      resolve()
    }
  })
}

function redirect(to, res) {
  res.writeHead(302, {
    'Location': to
  });
  res.end();
}

async function redirectToDrivingschool(db, req, res) {
  // PLZ aus Formular holen
  const body = await parsePostData(req)

  // GeoKoordinate für PLZ aus mongodb holen
  const postcode = await db.collection('postcodes').findOne({ postcode: body.postcode })

  // Die nächste Fahrschule im Umkreis von 10000km aus mongodb holen
  let drivingschool = null
  if (postcode) {
    const lng = postcode.coord.coordinates[0]
    const lat = postcode.coord.coordinates[1]
    const query = { coord: { $nearSphere: { $geometry: { type: "Point", coordinates: [ lng, lat ] }, $maxDistance: 10000000 }}}
    drivingschool = await db.collection('drivingschools').findOne(query)
  }

  // Redirect auf Fahrschule einleiten
  redirect(`http://${drivingschool ? drivingschool.website : process.env.DEFAULT_SITE}`, res)
}

async function responseClientFile(file, res) {
  const filePath = path.join(__dirname, 'client', file)
  const buf = await readFileAsync(filePath)
  res.statusCode = 200
  res.setHeader('Content-Type', mime.getType(filePath))
  res.end(buf);
}
