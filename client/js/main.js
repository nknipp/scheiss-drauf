
$(document).ready(function () {
  $('.input-lg').keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });

  $('button').click(function() {
    if ($('.input-lg').val().length < 5) {
      $('.alert').show();
      return false;
    }
  })
});